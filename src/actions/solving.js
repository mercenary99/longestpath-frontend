import axios from 'axios';
import {
  IS_SOLVING,
  SET_CORNER,
  SET_COUNTDOWN,
  SET_ID,
  SET_PATHINFO,
  SET_SETTINGS,
  SHOW_POPUP,
  START_SOLVING,
  STOP_SOLVING,
} from './types';

import corner1 from '../data/corner_z1.json';
import corner2 from '../data/corner_z2.json';
import corner3 from '../data/corner_z3.json';

/**
 * Set right URL
 */
const host =
  process.env.NODE_ENV === 'production'
    ? 'https://arya.ai'
    : 'http://localhost:8080';

/**
 * Generate a random id
 */
export const setId = () => (dispatch) => {
  dispatch({
    type: SET_ID,
    payload: Math.floor(Math.random() * 90000) + 10000,
  });
};

/**
 * Save pathinfos in state
 * @param {Object} infos
 */
export const setPathinfo = (infos) => async (dispatch) => {
  dispatch({
    type: SET_PATHINFO,
    payload: { ...infos },
  });
};

/**
 *
 * @param {String} name
 * @param {String} value
 */
export const setSettings = (name, value) => (dispatch) => {
  if (name === 'zone') {
    let corner = corner1;
    switch (value) {
      case 1:
        corner = corner1;
        break;
      case 2:
        corner = corner2;
        break;
      case 3:
        corner = corner3;
        break;
      default:
        corner = corner1;
        break;
    }
    dispatch({
      type: SET_CORNER,
      payload: corner,
    });
  }
  dispatch({
    type: SET_SETTINGS,
    payload: { [name]: value },
  });
};

/**
 * Set state to true or false
 * @param {boolean} show
 */
export const setShowPopup = (show) => (dispatch) => {
  dispatch({
    type: SHOW_POPUP,
    payload: show,
  });
};

/**
 * Starting Solving
 * @param {Object{id: number, zone: number}} body
 * @param {Object{terminationTime: number, heuristicType: string, localSearchType?: string}} idBody
 */
export const startSolving = (body, idBody) => async (dispatch) => {
  try {
    dispatch({
      type: IS_SOLVING,
    });
    await axios.post(host + '/api/options', body);
    dispatch({
      type: SET_COUNTDOWN,
    });
    const res = (await axios.post(host + '/api/solve', idBody)).data;
    dispatch({
      type: START_SOLVING,
      payload: res,
    });
    return res;
  } catch (err) {
    // console.error(err);
  }
};

/**
 * Stoping solving
 * @param {Object{id: number, zone: number}} idBody
 */
export const stopSolving = (idBody) => async (dispatch) => {
  try {
    await axios.post(host + '/api/stop', idBody);

    dispatch({
      type: STOP_SOLVING,
    });
  } catch (err) {
    console.error(err);
  }
};
