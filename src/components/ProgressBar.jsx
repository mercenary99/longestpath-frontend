import React from 'react';

const ProgressBar = ({ color, bgColor }) => {
  return (
    <div className={'bg-' + bgColor}>
      <div className={'progress-linear-' + color}></div>
    </div>
  );
};

export { ProgressBar };
