import React from 'react';
import { withRouter } from 'react-router';
// Images
import fhgr from '../assets/fhgr.png';
import optaPlanner from '../assets/opta_planner_logo.png';
import ost from '../assets/ost.png';

const FooterRouter = ({ location }) => {
  if (location.pathname === '/map') return <p></p>;
  return (
    <footer className='mt-1 py-1 bg-dark'>
      <div className='container-l flex-container m-auto space-between'>
        <div className='fhgr'>
          <a href='https://www.ost.ch/' target='_blank' rel='noreferrer'>
            <img src={ost} alt='OST Logo' />
          </a>
        </div>
        <div className=' text-center '>
          <p>
            created with <span className='text-danger'>&#9829;</span> by Rico
            Good und Erich Meyer
          </p>
          <p>
            <small>Bachelor thesis 2021 OST in coorperation with FHGR </small>
          </p>
          <small>
            Powered by{' '}
            <a
              href='https://www.optaplanner.org/'
              target='_blank'
              rel='noreferrer'
            >
              <img src={optaPlanner} alt='OptaPlaner Logo' />
            </a>
          </small>
        </div>
        <div className='fhgr'>
          <a href='https://www.fhgr.ch/' target='_blank' rel='noreferrer'>
            <img src={fhgr} alt='FHGR Logo' />
          </a>
        </div>
      </div>
    </footer>
  );
};

const Footer = withRouter(FooterRouter);
export { Footer };
