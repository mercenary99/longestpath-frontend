import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
// Components
import { NavLink } from 'react-router-dom';
import { ProgressBar } from './ProgressBar';
// Images
import logo from '../assets/logo.png';

const NavbarConnect = ({ isSolving }) => {
  return (
    <header className='hero bg-dark'>
      <div className='container-l flex-container space-between'>
        <NavLink to='/' exact>
          <div className='flex-container'>
            <img src={logo} alt='Logo' className='mr-1' />
            <h1>
              <span className='text-spezial'>Arya</span>{' '}
              <span className='text-light'>AI</span>
            </h1>
          </div>
        </NavLink>
        <nav className='menu'>
          <ul className='flex-container'>
            <li>
              <NavLink to='/' exact className='bg-dark nav-link'>
                Home
              </NavLink>
            </li>
            <li>
              <NavLink to='/infos' exact className='bg-dark nav-link'>
                Infos
              </NavLink>
            </li>
            <li>
              <NavLink to='/map' exact className='bg-dark nav-link'>
                Karte
              </NavLink>
            </li>
          </ul>
        </nav>
      </div>
      {isSolving ? (
        <ProgressBar color={'spezial'} bgColor={'dark'} />
      ) : (
        <div className='pt-1 bg-dark'></div>
      )}
    </header>
  );
};

// ----------------------------------
const mapStateToProps = (state) => ({
  isSolving: state.solving.isSolving,
});

NavbarConnect.protoTypes = {
  isSolving: PropTypes.bool.isRequired,
};

const Navbar = connect(mapStateToProps, null)(NavbarConnect);
export { Navbar };
