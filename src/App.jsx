import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import React, { Fragment, useEffect } from 'react';
// Components
import { Footer } from './components/Footer';
import { InfoContainer } from './pages/infos/InfoContainer';
import { Navbar } from './components/Navbar';
import { Landing } from './pages/landing/Landing';
import { Mapcontainer } from './pages/map/Mapcontainer';
// Actions
import { setId } from './actions/solving';
// Images
import './scss/App.scss';
import './scss/Mobile_App.scss';

/**
 *
 */
const App = ({ setId }) => {
  useEffect(
    () => setId(),
    // eslint-disable-next-line
    []
  );
  return (
    <Fragment>
      <Router>
        <Navbar />
        <Route exact path='/' component={Landing} />
        <Route exact path='/infos' component={InfoContainer} />
        <Route exact path='/map' component={Mapcontainer} />
        <Footer />
      </Router>
    </Fragment>
  );
};

// ----------------------------------
const mapActionToProps = {
  setId,
};

App.propTypes = {
  setId: PropTypes.func.isRequired,
};

export default connect(null, mapActionToProps)(App);
