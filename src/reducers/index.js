import { combineReducers } from 'redux';
import solving from './solving';

/**
 * Rootreducer combine all reducers
 */
export default combineReducers({
  solving,
});
