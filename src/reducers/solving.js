import {
  IS_SOLVING,
  SET_CORNER,
  SET_COUNTDOWN,
  SET_ID,
  SET_PATHINFO,
  SET_SETTINGS,
  SHOW_POPUP,
  START_SOLVING,
  STOP_SOLVING,
} from '../actions/types';

import corner1 from '../data/corner_z1.json';

const initialState = {
  corner: corner1,
  countdown: false,
  distance: 0,
  id: null,
  isSolving: false,
  response: {},
  score: '0',
  showPopup: false,
  visits: [],
  settings: {
    heuristic: 'STRONGEST_FIT',
    localSearch: 'TABU_SEARCH',
    time: 1,
    zone: 1,
  },
};

export default function solvingReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case IS_SOLVING:
      return {
        ...state,
        isSolving: true,
        visits: [],
        score: '0',
        distance: 0,
      };
    case SET_CORNER:
      return {
        ...state,
        corner: payload,
      };
    case SET_COUNTDOWN:
      return {
        ...state,
        countdown: !state.countdown,
      };
    case SET_ID:
      return {
        ...state,
        id: payload,
      };
    case SET_PATHINFO:
      return {
        ...state,
        ...payload,
      };
    case SET_SETTINGS:
      return {
        ...state,
        settings: {
          ...state.settings,
          ...payload,
        },
      };
    case START_SOLVING:
      return {
        ...state,
        response: { ...payload },
        isSolving: false,
      };
    case STOP_SOLVING:
      return {
        ...state,
        isSolving: false,
      };
    case SHOW_POPUP:
      return {
        ...state,
        showPopup: payload,
      };
    default:
      return state;
  }
}
