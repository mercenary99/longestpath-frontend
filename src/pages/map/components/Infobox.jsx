import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import React, { Fragment } from 'react';
// Component
import { Countdown } from './Countdown';
import { SettingSelector } from './SettingSelector';
// Action
import { setSettings } from '../../../actions/solving';
// Data
import selectorOptions from '../data/selectorOptions.json';

/**
 *
 */
const InfoboxConnect = ({
  distance,
  handleClick,
  isSolving,
  localSearch,
  score,
  setSettings,
  showPopup,
  time,
  zone,
}) => {
  return (
    <Fragment>
      <div className='mt-2 mx-3 mb-3'>
        <h2>Karte</h2>
        <p className='text-left'>
          Hier kannst du selber mitverfolgen, wie gut die verschiedenen
          Heuristiken und Local Searchs den längsten Weg finden. Kleiner Tipp,
          in unseren{' '}
          <Link to='/infos#benchmark'>Diagrammen der Benchmarks</Link> kannst du
          sehen, mit welchen es klappen könnte.
        </p>
      </div>

      <div className='mx-3 mb-2 infos-container'>
        <form>
          <div>
            <label id='zone-label' htmlFor='zone'>
              <span className='fw-500'>Zone:</span>
            </label>
            <select
              id='zone'
              name='zone'
              value={zone}
              onChange={(e) => setSettings(e.target.name, +e.target.value)}
              disabled={isSolving || showPopup}
            >
              <option value='1'>Zone 1</option>
              <option value='2'>Zone 2</option>
              <option value='3'>Zone 3</option>
            </select>
          </div>
          <div>
            <label id='time-label' htmlFor='time'>
              <span className='fw-500'>Zeit:</span>
            </label>
            <input
              id='time'
              type='number'
              value={time}
              name='time'
              min='1'
              max='30'
              step='1'
              onChange={(e) => setSettings(e.target.name, +e.target.value)}
              disabled={isSolving || showPopup}
            />
          </div>
          {localSearch === 'CUSTOM' ? (
            <div>
              <label htmlFor='heuristic'>
                <span className='fw-500'>Heuristik: </span>
              </label>
              <select
                id='heuristic'
                name='heuristic'
                onChange={(e) => setSettings(e.target.name, e.target.value)}
                disabled
              >
                <option value='STRONGEST_FIT'>Strongest Fit</option>
              </select>
            </div>
          ) : (
            <SettingSelector selectOption={selectorOptions[0]} />
          )}

          <SettingSelector selectOption={selectorOptions[1]} />
        </form>

        <div className='flex-container score-container'>
          <p className='my-1'>
            <span className='fw-500'>Distanz: </span> {distance}m
          </p>
          <p>
            <span className='fw-500'>Score: </span>
            <span
              className={
                /^-/gm.test(score)
                  ? 'text-danger'
                  : score === '0'
                  ? ''
                  : 'text-success'
              }
            >
              {score}
            </span>
          </p>
        </div>
      </div>
      {isSolving ? (
        <div className='stop-container'>
          <Countdown />

          <button
            className='btn btn-danger flat px-2 mb-2'
            onClick={() => handleClick('stop')}
          >
            Stop
          </button>
        </div>
      ) : (
        <button
          className={
            showPopup
              ? 'flat btn btn-success px-2 mb-2 disabled'
              : 'btn btn-success flat px-2 mb-2'
          }
          onClick={() => {
            handleClick('solve');
          }}
          disabled={showPopup}
        >
          Start
        </button>
      )}
    </Fragment>
  );
};

// ----------------------------------
const mapStateToProps = (state) => ({
  distance: state.solving.distance,
  isSolving: state.solving.isSolving,
  localSearch: state.solving.settings.localSearch,
  score: state.solving.score,
  showPopup: state.solving.showPopup,
  time: state.solving.settings.time,
  zone: state.solving.settings.zone,
});

const mapActionToProps = {
  setSettings,
};

InfoboxConnect.propTypes = {
  distance: PropTypes.number.isRequired,
  isSolving: PropTypes.bool.isRequired,
  localSearch: PropTypes.string.isRequired,
  score: PropTypes.string.isRequired,
  setSettings: PropTypes.func.isRequired,
  showPopup: PropTypes.bool.isRequired,
  time: PropTypes.number.isRequired,
  zone: PropTypes.number.isRequired,
};

const Infobox = connect(mapStateToProps, mapActionToProps)(InfoboxConnect);
export { Infobox };
