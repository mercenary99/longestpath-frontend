import polyline from '@mapbox/polyline';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Polyline } from 'react-leaflet';

/**
 *
 */
const PathConnect = ({ color, visits, weight, corner = null }) => {
  return (
    <Polyline
      color={color}
      positions={
        corner
          ? corner.map((v) => polyline.decode(v))
          : visits.map((v) => polyline.decode(v))
      }
      fill={false}
      weight={weight}
      opacity={0.6}
    />
  );
};

// ----------------------------------
const mapStateToProps = (state) => ({
  visits: state.solving.visits,
});

PathConnect.propTypes = {
  visits: PropTypes.array.isRequired,
};

const Path = connect(mapStateToProps, null)(PathConnect);
export { Path };
