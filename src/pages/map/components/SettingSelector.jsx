import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
// Action
import { setSettings } from '../../../actions/solving';

const SettingSelectorConnect = ({
  isSolving,
  selectOption,
  setSettings,
  settings,
  showPopup,
}) => {
  return (
    <div>
      <label htmlFor={selectOption.id}>
        <span className='fw-500'>{selectOption.name}: </span>
      </label>
      <select
        id={selectOption.id}
        value={settings[selectOption.id]}
        name={selectOption.id}
        onChange={(e) => setSettings(e.target.name, e.target.value)}
        disabled={isSolving || showPopup}
      >
        {selectOption.settings.map((s) => (
          <option key={s[0]} value={s[0]}>
            {s[1]}
          </option>
        ))}
      </select>
    </div>
  );
};
// ----------------------------------
const mapStateToProps = (state) => ({
  isSolving: state.solving.isSolving,
  settings: state.solving.settings,
  showPopup: state.solving.showPopup,
});

const mapActionToProps = {
  setSettings,
};

SettingSelectorConnect.propTypes = {
  isSolving: PropTypes.bool.isRequired,
  selectOption: PropTypes.object.isRequired,
  setSettings: PropTypes.func.isRequired,
  settings: PropTypes.object.isRequired,
  showPopup: PropTypes.bool.isRequired,
};

const SettingSelector = connect(
  mapStateToProps,
  mapActionToProps
)(SettingSelectorConnect);
export { SettingSelector };
