import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
// Actions
import { setShowPopup } from '../../../actions/solving';

/**
 *
 */
const InfoPopupConnect = ({ setShowPopup }) => {
  return (
    <div className='info-popup text-center'>
      <h1>Leider nein</h1>
      <p>
        Leider wurde mit diesen Einstellungen keine akzeptable Lösung gefunden
      </p>
      <p>
        Versuche es mit einer längeren Zeit oder spiele mit den verschiedenen
        Heuristiken und Local Search Einstellungen herum.
      </p>
      <button
        className='btn btn-danger flat px-2 mt-2'
        onClick={() => {
          setShowPopup(false);
        }}
      >
        Schliessen
      </button>
    </div>
  );
};

// ----------------------------------
const mapActionToProps = {
  setShowPopup,
};

InfoPopupConnect.propTypes = {
  setShowPopup: PropTypes.func.isRequired,
};

const InfoPopup = connect(null, mapActionToProps)(InfoPopupConnect);
export { InfoPopup };
