import PropTypes from 'prop-types';
import useCountDown from 'react-countdown-hook';
import { connect } from 'react-redux';
import React, { Fragment, useEffect } from 'react';

/**
 *
 */
const CountdownConnect = ({ countdown, time }) => {
  // Countdown Hook
  const [timeLeft, { start }] = useCountDown(time * 1000 * 60);

  useEffect(
    () => start(time * 1000 * 60),
    // eslint-disable-next-line
    [countdown]
  );

  let min = parseInt(timeLeft / 1000 / 60);
  let sec = (timeLeft / 1000) % 60;

  return (
    <Fragment>
      {min === 0 ? (
        <h3>{sec}s</h3>
      ) : (
        <h3>
          {min}:{sec}min
        </h3>
      )}
    </Fragment>
  );
};

// ----------------------------------
const mapStateToProps = (state) => ({
  countdown: state.solving.countdown,
  time: state.solving.settings.time,
});

CountdownConnect.propTypes = {
  countdown: PropTypes.bool.isRequired,
  time: PropTypes.number.isRequired,
};

const Countdown = connect(mapStateToProps, null)(CountdownConnect);
export { Countdown };
