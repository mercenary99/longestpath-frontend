import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Map, TileLayer } from 'react-leaflet';
// Componets
import { Path } from './Path';

/**
 *
 */
const MapsConnect = ({ corner }) => {
  return (
    <Map id='map' center={[46.87392, 9.5546]} zoom={16} scrollWheelZoom={true}>
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      />

      <Path corner={corner} color={'blue'} weight={'1'} />
      <Path color={'red'} weight={'3'} />
    </Map>
  );
};
// ----------------------------------
const mapStateToProps = (state) => ({
  corner: state.solving.corner,
});

MapsConnect.propTypes = {
  corner: PropTypes.array.isRequired,
};

const Maps = connect(mapStateToProps)(MapsConnect);
export { Maps };
