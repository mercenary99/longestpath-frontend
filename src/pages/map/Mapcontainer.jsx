import PropTypes from 'prop-types';
import useWebSocket from 'react-use-websocket';
import { connect } from 'react-redux';
import React, { Fragment, useEffect } from 'react';
// Components
import { Infobox } from './components/Infobox';
import { InfoPopup } from './components/InfoPopup';
import { Maps } from './components//Maps';
// Actions
import {
  setPathinfo,
  setShowPopup,
  startSolving,
  stopSolving,
} from '../../actions/solving';

const MapcontainerConnect = ({
  id,
  setPathinfo,
  setShowPopup,
  settings,
  showPopup,
  startSolving,
  stopSolving,
  zone,
}) => {
  const idBody = { id, zone };
  // Websocket
  const host =
    process.env.NODE_ENV === 'production'
      ? 'wss://arya.ai'
      : 'ws://localhost:8080';
  const socketUrl = `${host}/api/visits/${id}`;
  const { lastMessage } = useWebSocket(socketUrl, {
    shouldReconnect: (closeEvent) => false,
  });

  // Send the socket data to redux
  useEffect(
    () => setPathinfo(lastMessage && JSON.parse(lastMessage.data)),
    // eslint-disable-next-line
    [lastMessage]
  );

  /**
   * Handle start and stop
   */
  const handleClick = async (options) => {
    switch (options) {
      case 'solve':
        const body = {
          terminationTime: +settings.time,
          heuristicType: settings.heuristic,
          localSearchType: settings.localSearch,
        };
        if (settings.localSearch === 'CUSTOM') {
          delete body.localSearchType;
          delete body.heuristicType;
        }

        startSolving(body, idBody).then((res) => {
          if (res && !/0hard/gm.test(res.finalBestSolution.score))
            setShowPopup(true);
        });
        break;
      case 'stop':
        stopSolving(idBody);
        break;
      default:
        break;
    }
  };

  return (
    <Fragment>
      <div className='container-xll map-container text-center m-auto'>
        <div className='map-info'>
          <Infobox handleClick={handleClick} />
        </div>
        <div className='map'>
          <Maps />
        </div>
      </div>
      {showPopup && <InfoPopup />}
    </Fragment>
  );
};

// ----------------------------------
const mapStateToProps = (state) => ({
  id: state.solving.id,
  settings: state.solving.settings,
  showPopup: state.solving.showPopup,
  zone: state.solving.settings.zone,
});

const mapActionToProps = {
  setPathinfo,
  setShowPopup,
  startSolving,
  stopSolving,
};

MapcontainerConnect.propTypes = {
  id: PropTypes.number.isRequired,
  setPathinfo: PropTypes.func.isRequired,
  setShowPopup: PropTypes.func.isRequired,
  settings: PropTypes.object.isRequired,
  showPopup: PropTypes.bool.isRequired,
  startSolving: PropTypes.func.isRequired,
  stopSolving: PropTypes.func.isRequired,
  zone: PropTypes.number.isRequired,
};

const Mapcontainer = connect(
  mapStateToProps,
  mapActionToProps
)(MapcontainerConnect);

export { Mapcontainer };
