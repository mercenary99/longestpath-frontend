import React, { Fragment } from 'react';
// Components
import { Short } from './components/Short';
import { Title } from './components/Title';

const Landing = () => {
  return (
    <Fragment>
      <Title />
      <Short />
    </Fragment>
  );
};

export { Landing };
