import React from 'react';
import { Link } from 'react-router-dom';

const Short = () => {
  return (
    <div className='container-m text-center my-3'>
      <h2>Kurzte Erklärung</h2>
      <p className='text-left'>
        Kann man ein Longest Path Problem als Optimierungsproblem lösen? Das ist
        die grundlegende Frage, auf die wir in dieser Arbeit eingehen. Dazu
        verwenden wir OptaPlanner als Optimierungsengine, Open Source Routing
        Machine (ORSM) um die Distanzen zu bekommen und bauen eine Webseite mit
        React und Redux, um die Ergebnisse darzustellen.
      </p>
      <p className='mt-1 text-left'>
        Unser untersuchtes Gebiet ist ein Teil des Fürstenwaldes bei Chur.
        Zusätzlich unterteilten wird dieses noch einmal in drei verschieden
        grosse Gebiete, ein kleines für Tests, ein mittleres für Einstellungen
        der Heuristiken und Local Search Algorithmen und das komplette, wo wir
        den längst möglichen Weg finden wollen.
      </p>
      <p className='mt-1 text-left'>
        Die Schwierigkeit an diesem Ansatz ist, dass unser Domainmodel
        abgeleitet vom Traveling Salesman Problem (TSP) ist, wo immer alle
        "Knoten" (in unserem Fall sind das Kreuzungen) besucht werden müssen.
        Deshalb ist ein Hamiltonkreis nötig und ob wir einen solchen in unserem
        Gebiet finden würden, wussten wir am Anfang nicht.
      </p>
      <p className='mt-1 text-left'>
        Im kleinsten und mittleren Gebiet haben wir je einen Weg gefunden, der
        unseren Kriterien entspricht. In der Vollausprägung des Gebietes, haben
        wir auch nach mehreren Tests mit verschiedenen Konfigurationen kein
        akzeptables Resultat erhalten.
      </p>
      <p className='mt-1 text-left'>
        Grundsätzlich kann man sagen, dass sich Longest Path Probleme als
        Optimierungsprobleme gut lösen lassen, dabei gilt aber zu beachten, dass
        in dem Graphen ein Hamilton-Pfad vorhanden sein muss. Dies lässt sich
        aber nicht ohne weiteres im Voraus feststellen, da es ein
        NP-vollständiges Problem ist.
      </p>
      <Link to='/map' className='btn btn-success mt-4'>
        Zur Karte
      </Link>
    </div>
  );
};

export { Short };
