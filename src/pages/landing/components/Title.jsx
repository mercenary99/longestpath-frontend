import React from 'react';

const Title = () => (
  <div className='landing-content flex-container'>
    <div className='text-center title-container container-m text-light m-auto'>
      <h1>
        Künstliche Intelligenz für den längsten Trail durch den Fürstenwald
      </h1>
      <h2>Bachelorarbeit</h2>
      <p>von</p>
      <h3>Rico Good und Erich Meyer</h3>
    </div>
  </div>
);

export { Title };
