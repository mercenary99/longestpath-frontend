import React, { Fragment } from 'react';
// Images
import zone from '../../../assets/zone1_2_3.png';

const Infos = () => {
  return (
    <Fragment>
      <h1>Infos</h1>
      <h2>Einführung</h2>
      <p className='text-left'>
        In den letzten Jahren wurden online Kartendienste für die Gesellschaft
        immer wichtiger. Fast jeder benutzt sie, um den schnellsten oder
        kürzesten Weg von A nach B zu finden. Seit der Einführung des
        Smartphones können sie auch unterwegs genutzt werden und verdrängen so
        die mobilen GPS-Geräte, weil ihre Dienste vielfach kostenlos sind und
        mit den Jahren immer besser wurden.
      </p>
      <p className='mt-1 text-left'>
        Warum sollten wir jetzt den längsten Weg suchen, wo doch wirtschaftlich
        gesehen der kürzeste mehr Sinn ergibt? Ganz einfach, es soll nicht
        wirtschaftlich gesehen werden. Dieser Trail soll zur Erholung, für
        Training oder zum Spass benutzt werden. Wer wandert, joggt oder sonst
        Spass in der Natur hat, wird höchstwahrscheinlich nie den kürzesten Weg
        einschlagen und in der eigenen Freizeit werden sehr viele auch nicht an
        etwas wie Wirtschaftlichkeit denken.
      </p>
      <p className='mt-1 text-left'>
        Wenn nun jemand den längsten Trail sucht, wird er vermutlich eine Karte
        nehmen und von Hand den geschätzt umfangreichsten Weg einzeichnen. Hier
        setzt unsere Idee an. Wir möchten in einem Teil des Fürstenwaldes
        herausfinden, ob es möglich ist mit dem Ansatz eines
        Optimierungsproblems den längsten Weg in diesem Gebiet zu finden.
      </p>

      <h2>Fazit</h2>
      <div className='flex-container'>
        <div className='mr-2'>
          <p className='text-left'>
            Wie sich herausstellte, ist es nicht ganz einfach den längsten Weg
            zu finden, wenn man jeden einzelnen Knoten besuchen möchte. Hierbei
            liegt die Schwierigkeit, dass es für eine solche Suche mindestens
            einen Hamiltonkreis gibt.
          </p>
          <p className='mt-1 text-left'>
            Wie aus den Ergebnissen dem kleinen und mittleren Teilgebiete
            ersichtlich ist, besteht aber durchaus die Möglichkeit dafür.
          </p>
          <p className='mt-1 text-left'>
            Für das Gebiet <span className='serif'>I</span> findet sich mit der
            richtigen Local Search Methode, innerhalb von 4.2s ein Weg von
            3076.3m. Und das mit den Defaulteinstellungen des Local Search
            Algorithmus in Kombination mit der Construction Heuristic Strongest
            Fit. Um noch einen anderen Ansatz auszuprobieren, den wir in dem
            Traveling Salesman Beispiel von OptaPlanner finden, versuchten wir
            es noch mit Late Acceptance. Auf Anhieb ist er nicht so erfolgreich
            wie Tabu Search, aber mit dem richtigen Finetuning findet auch Late
            Acceptance den gleichen Weg wie Tabu Search. Er braucht dafür aber
            ungefähr zehnmal so lange 43.8s.
          </p>

          <p className='mt-1 text-left'>
            Im Gebiet <span className='serif'>II</span> kommt der Erfolgt nicht
            so einfach. Mit den Defaulteinstellungen aller Algorithmen finden
            wir hier keinen für uns akzeptablen Weg. Als bestes schneidet wieder
            Tabu Search ab. Darum versuchten wir diesen anschliessend zu
            verbessern. Bei den verschiedenen Typen zeigt sich, dass Undo Move
            Tabu Size mit -1 Hard Constraint am besten abschneidet. Aber auch
            mit verschiedenen Konfigurationen, wo die Tabu Size und das Accepted
            Counter Limit, anders eingestellt sind, wird immer mindestens eine
            Regel gebrochen. Wahrscheinlich bleibt Tabu Search in einem lokalen
            Optimum stecken. Mit unserem Custom Late Acceptance ist das Ergebnis
            positiver. Durch den changeMoveSelector erhält er noch ein bischen
            Zufälligkeit, welche helfen kann, ein Steckenbleiben zu verhindern.
            Nach 187.7s wurde eine Lösung in unserem Sinne gefunden.
          </p>
          <p className='mt-1 text-left'>
            Auf das Gebiet <span className='serif'>III</span> angewendet, findet
            sich keine akzeptable Lösung. Weder mit den Kombinationen aus allen
            Algorithmen, verschiedenen Tabu Search Konfigurationen, noch mit
            unserem optimierten Late Acceptance. Nach wenigen Minuten, bei Tabu
            Search 391.2s und Late Acceptance 643.3s, sind beide Algorithmen auf
            ihrem jeweiligen Optimum angekommen und im weiteren Verlauf dieser
            24 Stunden Benchmarks verändert sich nichts mehr. Es ist durchaus
            möglich, dass es keinen Hamiltonkreis in diesem Gebiet gibt. Mit
            abschliessender Sicherheit kann man das aber nicht sagen. Um jede
            Möglichkeit durchzuprobieren würde es{' '}
            <span className='serif'>
              3.8 &#183; 10 <sup>118</sup>
            </span>{' '}
            Jahre dauern, soviel Zeit können wir uns leider nicht nehmen.
          </p>
        </div>
        <div>
          <img src={zone} alt='Gebitsgrenzen' />
          <p className='text-center'>
            <small>Gebietsbegrenzung Übersicht</small>
          </p>
        </div>
      </div>
    </Fragment>
  );
};

export { Infos };
