import { Bar } from 'react-chartjs-2';
import React, { useState } from 'react';
// Data
import benchmark1JSON from '../../../data/bm_z1.json';
import benchmark2JSON from '../../../data/bm_z2.json';
import benchmark3JSON from '../../../data/bm_z3.json';

const Diagram = () => {
  const [zone, setZone] = useState(1);
  const benchmark = [
    benchmark1JSON.data,
    benchmark2JSON.data,
    benchmark3JSON.data,
  ];

  let data = {
    labels: [],
    datasets: [
      {
        label: '# Hard Score',
        data: [],
        backgroundColor: '#212121',
        yAxisID: 'y-axis-1',
      },
      {
        label: '# Soft Score',
        data: [],
        backgroundColor: '#fbc02d',
      },
    ],
  };

  const options = {
    scales: {
      yAxis: [
        {
          type: 'Linear',
          reverse: true,
          display: true,
          position: 'left',
          id: 'y-axis-1',
          gridLines: {
            drawOnArea: true,
          },
          ticks: {
            beginAtZero: true,
          },
        },
        {
          type: 'linear',
          display: true,
          position: 'right',
          id: 'y-axis-2',
          gridLines: {
            drawOnArea: true,
          },
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  };

  benchmark[zone - 1].forEach((b) => {
    data.labels.push(
      b[0]
        .replace(/_/g, ' ')
        .toLocaleLowerCase()
        .replace(/(^\w|\s\w)/g, (m) => m.toUpperCase())
        .replace('Custom', 'CUSTOM')
    );
    data.datasets[0].data.push(b[1]);
    data.datasets[1].data.push(b[2]);
  });

  return (
    <div id='benchmark'>
      <h1>Benchmarks</h1>
      <select
        id='zone'
        name='zone'
        value={zone}
        onChange={(e) => setZone(e.target.value)}
      >
        <option value='1'>Zone 1</option>
        <option value='2'>Zone 2</option>
        <option value='3'>Zone 3</option>
      </select>
      <Bar data={data} options={options} />
    </div>
  );
};

export { Diagram };
