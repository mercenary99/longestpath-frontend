import React from 'react';
// Components
import { Diagram } from './components/Diagram';
import { Infos } from './components/Infos';

const InfoContainer = () => {
  return (
    <main className='container-l info-container px-2'>
      <Diagram />
      <Infos />
    </main>
  );
};

export { InfoContainer };
